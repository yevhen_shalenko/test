import {
  IonContent,
  IonItem,
  IonSelect,
  IonButton,
  IonSelectOption,
} from '@ionic/react';
import './History.css';
import Filter from '../Filter/Filter';
import { useEffect, useState } from 'react';
import FilterBtn from '../FilterBtn/FilterBtn';

const History: React.FC = () => {
  const [historyArr, setHistoryArr] = useState<any>([]);
  const [plug, setPlug] = useState<any>([]);

  const [sort, setSort] = useState<string>('dateUp');
  const [startDate, setStartDate] = useState<any>();
  const [endDate, setEndDate] = useState<any>();

  useEffect(() => {
    const lsHistory = localStorage.getItem('history');
    const parsedLsHistory = JSON.parse(lsHistory as string);
    setPlug(parsedLsHistory);
    setHistoryArr(parsedLsHistory);
  }, []);

  useEffect(() => {
    setPlug(historyArr);
  }, [historyArr]);

  useEffect(() => {
    switch (sort) {
      case 'dateUp':
        const dateUp = [...plug].sort((a: any, b: any) => a.date - b.date);
        setPlug(dateUp);
        break;
      case 'dateDown':
        const dateDown = [...plug].sort((a: any, b: any) => b.date - a.date);
        setPlug(dateDown);
        break;
      case 'currencyUp':
        const currencyUp = [...plug].sort((a: any, b: any) =>
          a.currency.localeCompare(b.currency),
        );
        setPlug(currencyUp);
        break;
      case 'currencyDown':
        const currencyDown = [...plug].sort((a: any, b: any) =>
          b.currency.localeCompare(a.currency),
        );
        setPlug(currencyDown);
        break;
    }
  }, [sort]);

  const filterSetPlug = (value: any) => {
    setPlug(value);
  };
  const filterSetStartDate = (value: any) => {
    setStartDate(value);
  };
  const filterSetEndDate = (value: any) => {
    setEndDate(value);
  };

  return (
    <IonContent fullscreen>
      <div className="container">
        <h2 className="container-title">Transaction history</h2>
        <div className="table">
          <div className="sort-box">
            <FilterBtn
              historyArr={historyArr}
              plug={plug}
              setPlug={filterSetPlug}
              filterStartDate={filterSetStartDate}
              filterEndDate={filterSetEndDate}
            />
            <IonItem>
              <IonSelect
                onIonChange={(e: any) => setSort(e.currentTarget.value)}
                interface="popover"
                value={sort}
              >
                <IonSelectOption value="dateUp">Date Start</IonSelectOption>
                <IonSelectOption value="dateDown">Date Down</IonSelectOption>
                <IonSelectOption value="currencyUp">
                  Currency Start
                </IonSelectOption>
                <IonSelectOption value="currencyDown">
                  Currency Down
                </IonSelectOption>
              </IonSelect>
            </IonItem>
          </div>
          <div className="timePeriodBox">
            <p className="timePeriodFrom">
              From: {startDate && new Date(startDate).toLocaleString()} Till:{' '}
              {endDate && new Date(endDate).toLocaleString()}
            </p>
          </div>
          <Filter historyArr={historyArr} plug={plug} />
        </div>
      </div>
    </IonContent>
  );
};
export default History;
