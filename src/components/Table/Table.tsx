import './Table.css';
import MaterialTable from 'material-table';
import tableIcons from './IconProvider';
import PrintIcon from '@material-ui/icons/Print';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import XLSX from 'xlsx';
import { useEffect, useState } from 'react';
interface ContainerProps {
  historyArr: any;
  plug: any;
}

const Table: React.FC<ContainerProps> = ({ historyArr, plug }) => {
  const [data, setData] = useState<any>([]);
  const [loaded, setLoaded] = useState(false);
  const [selected, setSelected] = useState<string>('');
  console.log('selected>>', selected);

  const showDateTime = (dateTime: number) => {
    if (dateTime) {
      return new Date(dateTime).toLocaleString();
    }
  };
  useEffect(() => {
    if (data.length !== plug.length) {
      const result = plug.filter((item: any) => item.id !== selected);

      localStorage.setItem('history', JSON.stringify(result));
    }
  }, [data]);

  const columns = [
    {
      title: 'Currency',
      field: 'currency',
    },
    {
      title: 'Entered Amount',
      field: 'enteredAmount',
    },
    {
      title: 'Amount',
      field: 'amount',
    },
    {
      title: 'Rate',
      field: 'rate',
    },
    {
      title: 'Date',
      field: 'date',
    },
  ];

  plug &&
    plug.forEach((item: any) => {
      if (loaded) return;
      const date = showDateTime(item.date);
      const obj: any = { ...item, date };
      setData((prev: any) => [...prev, obj]);
      setLoaded(true);
      // console.log('data deploy');
    });
  // console.log('data', data);

  const downloadPdf = () => {
    const doc = new jsPDF();
    doc.text('Table History', 20, 10);
    autoTable(doc, {
      columns: columns.map(col => ({ ...col, dataKey: col.field })),
      body: data,
    });

    doc.save('table.pdf');
  };

  return (
    <div>
      <MaterialTable
        columns={columns}
        data={data}
        icons={tableIcons}
        title="History Table"
        actions={[
          {
            icon: () => <PrintIcon />,
            tooltip: 'Export to Pdf',
            onClick: () => downloadPdf(),
            isFreeAction: true,
          },
        ]}
        options={{
          exportButton: true,
          sorting: true,
          actionsColumnIndex: -1,
        }}
        editable={{
          onRowDelete: selectedRow =>
            new Promise<void>((resolve, reject) => {
              const index: any = selectedRow.tableData;
              console.log(selectedRow.id);
              const idx: any = selectedRow.id;
              setSelected(idx);
              const { id } = index;
              const updatedRows = [...data];
              updatedRows.splice(id, 1);
              setTimeout(() => {
                setData(updatedRows);
                resolve();
              }, 1000);
            }),
        }}
      />
    </div>
  );
};

export default Table;
