import './FilterBtn.css';
import {
  IonButton,
  IonDatetime,
  IonItem,
  IonLabel,
  useIonAlert,
  IonModal,
} from '@ionic/react';
import { useState } from 'react';

interface ContainerProps {
  historyArr: any;
  plug: any;
  setPlug: any;
  filterStartDate: any;
  filterEndDate: any;
}

const Filter: React.FC<ContainerProps> = ({
  historyArr,
  plug,
  setPlug,
  filterEndDate,
  filterStartDate,
}) => {
  const [showModal, setShowModal] = useState(false);
  const [startDate, setStartDate] = useState<any>();
  const [endDate, setEndDate] = useState<any>();
  const [present] = useIonAlert();

  const closeModal = () => {
    const reserv = historyArr;
    const begin = Number(new Date(startDate));
    const end = Number(new Date(endDate));
    if (plug.length !== historyArr.length) {
      const result = reserv.filter((item: any) => {
        return item.date >= begin && item.date;
      });
      console.log('result', result);
      setShowModal(false);
      return setPlug(result);
    }

    if (!begin || !end) {
      return present({
        header: 'Warning!',
        message: 'Choose date!',
        buttons: [{ text: 'Ok' }],
      });
    }
    const result = plug.filter((item: any) => {
      return item.date >= begin && item.date < end;
    });
    setPlug(result);
    setShowModal(false);
  };

  return (
    <>
      <IonButton onClick={() => setShowModal(true)}>Filter</IonButton>
      <IonModal isOpen={showModal} cssClass="my-custom-class">
        <div className="modalFilterDate">
          <IonItem>
            <IonLabel>Start Date</IonLabel>
            <IonDatetime
              displayFormat="MMM DD, YYYY HH:mm"
              placeholder="Select Date"
              value={startDate}
              onIonChange={e => {
                setStartDate(e.detail.value!);
                filterStartDate(e.detail.value!);
              }}
            >
              <></>
            </IonDatetime>
          </IonItem>
          <IonItem>
            <IonLabel>End Date</IonLabel>
            <IonDatetime
              displayFormat="MMM DD, YYYY HH:mm"
              placeholder="Select Date"
              value={endDate}
              onIonChange={e => {
                setEndDate(e.detail.value!);
                filterEndDate(e.detail.value!);
              }}
            >
              <></>
            </IonDatetime>
          </IonItem>
        </div>

        <IonButton onClick={closeModal} className="close-modal-btn">
          Close Modal
        </IonButton>
      </IonModal>
    </>
  );
};

export default Filter;
