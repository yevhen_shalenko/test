import {
  IonButton,
  IonInput,
  IonItem,
  IonLabel,
  IonSelect,
  IonSelectOption,
  IonContent,
  IonSpinner,
  useIonAlert,
} from '@ionic/react';
import './Dev.css';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';

const Dev: React.FC = () => {
  const [number, setNumber] = useState<number>();
  const [present] = useIonAlert();
  const [newNumber, setNewNumber] = useState<number>(0);
  const [buyCurrency, setBuyCurrency] = useState<string>('UAH');
  const [saleCurrency, setSaleCurrency] = useState<string>('USD');
  const [currency, setCurrency] = useState<any>({});
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<string>('');
  const lsHistory = localStorage.getItem('history');

  const [historyArr, setHistoryArr] = useState<any>(
    JSON.parse(lsHistory as string) || [],
  );

  const URL =
    'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5';
  const falseURL =
    'https://api.privatbank.ua/p24api/pubi1nfo?json&exchange&coursid=5';

  const currencyMap: any = {
    UAHUSD: null,
    USDUAH: null,
    UAHEUR: null,
    EURUAH: null,
    EURUSD: null,
    USDEUR: null,
  };

  const getBasicCurrencyMap = (item: any) => {
    item.forEach((item: any) => {
      if (item.ccy === 'USD') {
        currencyMap.UAHUSD = 1 / Number(item.sale);
        currencyMap.USDUAH = Number(item.buy);
      }
      if (item.ccy === 'EUR') {
        currencyMap.UAHEUR = 1 / Number(item.sale);
        currencyMap.EURUAH = Number(item.buy);
      }
      currencyMap.EURUSD = currencyMap.EURUAH * currencyMap.UAHUSD;
      currencyMap.USDEUR = currencyMap.USDUAH * currencyMap.UAHEUR;
    });
    return currencyMap;
  };

  const setCurrencyFromApi = async (url: string) => {
    setError('');
    setLoading(true);
    try {
      const currencyArr = await axios.get(`${url}`);
      const getBasicCurrencyMapRes = getBasicCurrencyMap(currencyArr.data);
      console.log('getBasicCurrencyMapRes', getBasicCurrencyMapRes);
      setTimeout(() => {
        setLoading(false);
      }, 1000);
      setCurrency(currencyMap);

      localStorage.setItem('currency', JSON.stringify(currencyMap));
      const currentDate = Date.now();
      localStorage.setItem('date', JSON.stringify(currentDate));
    } catch (err: any) {
      setLoading(false);
      setError(err.message);
    }
  };

  const getLocalStorageCurrency = () => {
    const lsData = localStorage.getItem('currency');
    if (!lsData) {
      return null;
    }
    return JSON.parse(lsData);
  };

  const getLocalStorageDate = () => {
    const lsData = localStorage.getItem('date');
    const savedData = JSON.parse(lsData as string);
    return Date.now() - savedData > 86400000;
  };

  const getLSHistory = () => {
    const lsHistory = localStorage.getItem('history');
    const res = JSON.parse(lsHistory as string);
    return res;
  };

  useEffect(() => {
    const storageCurrency = getLocalStorageCurrency();
    const storageDate = getLocalStorageDate();
    getLSHistory() && setHistoryArr(getLSHistory());
    if (!storageCurrency || storageDate) {
      setCurrencyFromApi(URL);
    } else {
      setCurrency(storageCurrency);
    }
  }, []);

  useEffect(() => {
    if (historyArr.length > 0) {
      localStorage.setItem('history', JSON.stringify(historyArr));
    }
  }, [historyArr]);

  const onHandleChange = (e: any) => {
    setNumber(e.currentTarget.value);
    e.currentTarget.value === '' && setNewNumber(0);
  };

  const onHandleClick = () => {
    let amountNew: any;
    let currencyRate: any;

    if (buyCurrency === '' || saleCurrency === '') {
      return present({
        header: 'Warning!',
        message: 'Select currency',
        buttons: [{ text: 'Ok' }],
      });
    }

    if (number === 0 || number === undefined) {
      return present({
        header: 'Warning!',
        message: 'Enter the amount',
        buttons: [{ text: 'Ok' }],
      });
    }

    if (saleCurrency === buyCurrency) {
      present({
        header: 'Warning!',
        message: 'Choose another currency',
        buttons: [{ text: 'Ok' }],
      });
    } else {
      const keys = Object.keys(currency);

      for (const key of keys) {
        const concatCurrency = `${buyCurrency}${saleCurrency}`;
        if (key === concatCurrency) {
          amountNew = ((number || 0) * currency[key]).toFixed(2);
          setNewNumber(Number(amountNew));
          currencyRate = currency[key];
        }
      }
    }

    setHistoryArr((prev: any) => [
      ...prev,
      {
        id: uuidv4(),
        currency: `${buyCurrency}/${saleCurrency}`,
        // saleCurrency: saleCurrency,
        amount: amountNew,
        enteredAmount: number,
        date: Date.now(),
        rate: Number(currencyRate).toFixed(2),
      },
    ]);
  };

  const showError = () => {
    return (
      <div className="container">
        <h2 className="errorTitle">{error}</h2>
        <IonButton
          className="refreshBtn"
          onClick={() => setCurrencyFromApi(URL)}
        >
          Refresh Page
        </IonButton>
      </div>
    );
  };

  return (
    <IonContent fullscreen>
      {error.length > 0 ? (
        showError()
      ) : loading ? (
        <div className="spinner-container">
          <IonSpinner />
        </div>
      ) : (
        <div className="container">
          <h2 className="ion-text-center">Converter</h2>
          <div className="currencyBox">
            <IonItem>
              <IonLabel position="floating">Buy</IonLabel>
              <IonSelect
                onIonChange={(e: any) => {
                  setBuyCurrency(e.currentTarget.value);
                }}
                value={buyCurrency}
                interface="popover"
                name="buyCurrency"
              >
                <IonSelectOption value="UAH">UAH</IonSelectOption>
                <IonSelectOption value="USD">USD</IonSelectOption>
                <IonSelectOption value="EUR">EUR</IonSelectOption>
              </IonSelect>
            </IonItem>

            <IonItem className="ion-item-param">
              <IonLabel position="floating">Sale</IonLabel>
              <IonSelect
                onIonChange={(e: any) => setSaleCurrency(e.currentTarget.value)}
                value={saleCurrency}
                name="saleCurrency"
                interface="popover"
              >
                <IonSelectOption value="UAH">UAH</IonSelectOption>
                <IonSelectOption value="USD">USD</IonSelectOption>
                <IonSelectOption value="EUR">EUR</IonSelectOption>
              </IonSelect>
            </IonItem>
          </div>

          <IonItem className="amountStyle">
            <IonLabel position="floating">Enter the amount</IonLabel>
            <IonInput
              onIonInput={onHandleChange}
              value={number}
              name="enteredAmount"
              type="number"
            />
          </IonItem>
          <IonButton className="btn" onClick={onHandleClick} type="button">
            Convert
          </IonButton>
          <IonItem className="result">
            <p className="resultText">{newNumber}</p>
          </IonItem>
        </div>
      )}
    </IonContent>
  );
};

export default Dev;
