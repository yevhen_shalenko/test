import { IonButton } from '@ionic/react';
import Table from '../Table/Table';

interface ContainerProps {
  historyArr: any;
  plug: any;
}

const Filter: React.FC<ContainerProps> = ({ historyArr, plug }) => {
  return (
    <>
      <Table historyArr={historyArr} plug={plug} />
    </>
  );
};

export default Filter;
